#!/bin/sh


rm -rf .git
git init

# git remote add origin git@git.oschina.net:kitech/aurcare.git
# git remote add origin git@gitlab.com:kitech/aurcare.git
cp -v dotgitconfig .git/config

git remote -v


repo-add aurcare.db.tar.gz *.xz
unlink aurcare.db
unlink aurcare.files
cp -v aurcare.db.tar.gz aurcare.db
cp -v aurcare.files.tar.gz aurcare.files


git add -v readme.txt gcgitdb.sh dotgitconfig aurcare.db* aurcare.files* fonts pkgs
git add -v new_ver.txt nvchecker.ini old_ver.txt
git commit -a -m "gcgitdb... inited"
git push origin master --force

echo "Pushed basic files."
sleep 10

for f in `ls *.tar.xz`; do
    trycnt=0
    while true; do
        git add -v "$f"
        git commit -a -m "gcgitdb... readd $f"
        git push origin master
        #if [ x"$?" != x"0" ]; then
        #    exit -1;
        #fi
        ret=$?
        if [ x"$ret" = x"0" ]; then
            break;
        else
            echo "Push error $trycnt,..."
            sleep 5;
        fi
        trycnt=`expr $trycnt + 1`
    done
done

#repo-add aurcare.db.tar.gz *.xz
#unlink aurcare.db
#unlink aurcare.files
#cp -v aurcare.db.tar.gz aurcare.db
#cp -v aurcare.files.tar.gz aurcare.files

